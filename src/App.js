import React, { Component } from 'react';
import { Hexagon, HexGrid, Layout, Pattern, Text } from 'react-hexgrid';

import './App.scss';

const buildings = require('./data/buildings');
const terrains = require('./data/terrains');

class App extends Component {

    componentWillMount() {
        var hexagons = [],
            qS = -11,
            rS = 1,
            sS = 0,
            qStart = qS,
            rStart = rS,
            sStart = sS,
            q = qStart - 2,
            r = rStart + 1,
            s = sStart;
        for (var index = 0; index < 216; index++) {
            var hexagonHelper = new Array();
            hexagonHelper['id'] = index;
            hexagonHelper['building'] = null;
            hexagonHelper['terrain'] = 0;
            q += 2;
            r -= 1;
            if (index > 0 && index % 12 === 0) {
                if (index % 24 === 0) {
                    qStart = qS + 1;
                    rStart = rS;
                    qS = qStart - 1;
                    rS = rStart + 1;
                }
                q = qStart - 1;
                r = rStart + 1;
                qStart = q;
                rStart = r;
            }
            hexagonHelper['q'] = q;
            hexagonHelper['r'] = r;
            hexagonHelper['s'] = s;
            hexagons[index] = hexagonHelper;
        }
        this.setState({
            blocks: [
                {
                    img: 'images/html.svg',
                    id: 'Block1',
                    name: 'Block1',
                    price: 10,
                    quantity: 0,
                    cash: 0.1,
                },
                {
                    id: 'Block2',
                    name: 'Block2',
                    price: 15,
                    quantity: 0,
                    cash: 0.2,
                },
            ],
            building: 1,
            cash: 20,
            currency: '$',
            loading: true,
            height: 0,
            hexagons: hexagons,
            priceRate: 0.25,
            width: 0,
        });
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
        this.addCash = this.addCash.bind(this);
    }

    componentDidMount() {
        this.interval = setInterval(() => this.addCash(), 1000);
        this.setState({
            loading: false,
        });
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({width: window.innerWidth, height: window.innerHeight});
    }

    addCash() {
        this.state.hexagons.forEach((hexagon) => {
            if (hexagon.building) {
                var newCash = this.state.cash + (buildings[hexagon.building].income * hexagon.level);
                this.setState({
                    cash: newCash
                });
            }
        });
    }

    handleClick(hexagonIndex) {
        var hexagons = this.state.hexagons,
            hexagon = hexagons[hexagonIndex],
            newCash = this.state.cash - ((buildings[this.state.building].cost * hexagon.level || 0) + buildings[this.state.building].cost);
        // if ((newCash >= 0) && (hexagon['building'] !== this.state.building)) {
        if (newCash >= 0) {
            hexagon['building'] = this.state.building;
            hexagon['level'] = (hexagon['level'] || 0) + 1;
            hexagons[hexagonIndex] = hexagon;
            this.setState({
                hexagons: hexagons,
                cash: newCash
            });
        }
    }

    render() {
        return (
            <div className='App'>
                <div className={'cash'}>
                    {parseFloat(this.state.cash).toFixed(2)}
                </div>
                <HexGrid width={this.state.width} height={this.state.height} viewBox="-50 -50 100 100">
                    <Layout size={{x: 5, y: 5}} flat={true} spacing={1} origin={{x: 0, y: 0}}>
                        {this.state.hexagons.map((hexagon, index) => {
                            return (
                                <Hexagon q={hexagon.q} r={hexagon.r} s={hexagon.s} fill={`terrain-${hexagon.terrain}`}
                                         onClick={this.handleClick.bind(this, index)} key={index}/>
                            );
                        })}
                        {this.state.hexagons.map((hexagon, index) => {
                            if (!hexagon.building) {
                                return null;
                            }
                            return (
                                <Hexagon q={hexagon.q} r={hexagon.r} s={hexagon.s} fill={`building-${hexagon.building}`}
                                         onClick={this.handleClick.bind(this, index)} key={index}>
                                </Hexagon>
                            );
                        })}
                        {/*<Path start={new Hex(0, 0, 0)} end={new Hex(-2, 0, 1)}/>*/}
                    </Layout>
                    {Object.keys(terrains).map((index) => {
                        var terrain = terrains[index],
                            image = 'images/' + terrain.image + '.png';
                        return (
                            <Pattern id={`terrain-${index}`} key={`terrain-${index}`} link={image}/>
                        );
                    })}
                    {Object.keys(buildings).map((index) => {
                        var building = buildings[index],
                            image = 'images/' + building.image + '.png';
                        return (
                            <Pattern id={`building-${index}`} key={`building-${index}`} link={image}/>
                        );
                    })}
                </HexGrid>
            </div>
        );
    }
}

export default App;
