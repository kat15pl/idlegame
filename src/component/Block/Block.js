import React, { Component } from 'react';
import './Block.scss';

class Block extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: props.block.id,
            text: props.block.name,
            price: props.block.price,
            quantity: props.block.quantity,
            currency: props.block.currency,
            cash: props.block.cash,
            img: props.block.img,
        };
        this.buy = this.buy.bind(this);
    }

    buy(event) {
        this.props.buy(this.state.id, this);
    }

    render() {
        return (
            <div className='block' onClick={this.buy} style={{ background: `url(${this.state.img})` }}>
                <div className='name'>
                    {this.state.text}
                </div>
                <div className='price'>
                    ({this.state.price} {this.state.currency})
                </div>
                <div className='quantity'>
                    {this.state.quantity}
                </div>
            </div>
        );
    }
}

export default Block;
